angular.module('studentApp').factory('StudentService', StudentService);

function StudentService() {
	var oStudent = {};
	oStudent.getStudents = getStudents;
	oStudent.addStudent = addStudent;
	oStudent.editStudent = editStudent;
	oStudent.deleteStudent = deleteStudent;
	oStudent.maxId = maxId;

	var students = [
		{
			id: 1,
			name: 'Raia',
			Facultativ: 'Securitate WEB',
			class: 1901,
			mobile: '236-22-768909'
		},
		{
			id: 2,
			name: 'Nina',
			Facultativ: 'Securitate WEB',
			class: 1900,
			mobile: '236-22-613456'
		},
		{
			id: 3,
			name: 'Vasile',
			Facultativ: 'Securitate BD',
			class: 1901,
			mobile: '236-22-789012'
		},
		{
			id: 4,
			name: 'Arteom',
			Facultativ: 'Securitate BD',
			class: 1902,
			mobile: '236-22-129474'
		},
		{
			id: 5,
			name: 'Nina',
			Facultativ: 'Securitate WEB',
			class: 1902,
			mobile: '236-22-124385'
		}
	];

	function getStudents() {
		return students;
	}

	function addStudent(student) {
		students.push(student);
	}

	function editStudent(student) {
		for (var i = 0; i < students.length; i++) {
			if (students[i].id == student.id) {
				students[i].name = student.name;
				students[i].Facultativ = student.Facultativ;
				students[i].class = student.class;
				students[i].mobile = student.mobile;
				break;
			}
		}
	}

	function deleteStudent(sid) {
		for (var i = 0; i < students.length; i++) {
			if (students[i].id == sid) {
				var deleteUser = students[i].name;
				var isconfirm = confirm(
					'Sunteti siguri ca vreti sa-l stergeti pe studentul ' + deleteUser
				);
				if (isconfirm) {
					students.splice(i, 1);
					alert(deleteUser + ' a fost sters cu succes');
				}
				break;
			}
		}
	}

	function maxId() {
		var max = 0;
		for (var i = 0; i < students.length; i++) {
			if (students[i].id > max) {
				max = students[i].id;
			}
		}
		return max;
	}

	return oStudent;
}
